# SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import logging

from lib.project import Project
from lib.member import Member
from lib.sharedwithgroup import SharedWithGroup

class Group:

    def __init__(self, gl, id):
        self.gl = gl
        self.id = id
        self.obj = self.gl.groups.get(self.id)
        self.name = self.obj.name
        self.web_url = self.obj.web_url
        logging.info(f"Processing Group {self.name} / {self.id}")
        self.projects = []
        self.subgroups = []
        self.member_users = Member(gl, self.obj)

        self.member_groups = []
        for shared_with_group in self.obj.shared_with_groups:
            self.member_groups.append(SharedWithGroup(shared_with_group))

        # https://docs.gitlab.com/ee/api/group_access_tokens.html
        self.accesstokens = []
        for accesstoken in self.obj.access_tokens.list():
            if accesstoken.active and not accesstoken.revoked:
                self.accesstokens.append(accesstoken)

        for project in self.obj.projects.list(get_all=True):
            project_obj = Project(self.gl, project.id, self.obj.full_path)
            if project_obj.in_expected_namespace:
                self.projects.append(project_obj)

        for group in self.obj.subgroups.list(get_all=True):
            group_obj = Group(self.gl, group.id)
            self.subgroups.append(group_obj)

        logging.debug(f"- {self.name} User Members: "+str(sum(1 for _ in self.member_users)))
        logging.debug(f"- {self.name} Group Members: "+str(sum(1 for _ in self.member_groups)))
        logging.debug(f"- {self.name} Subgroups: "+str(sum(1 for _ in self.subgroups)))
        logging.debug(f"- {self.name} Projects: "+str(sum(1 for _ in self.projects)))

    def process_tree(self, callback_function, content=None, skip_projects=False):
        # it's call by reference so we do not need to return the value
        callback_function(self, content=content)
        for subgroup in self.subgroups:
            subgroup.process_tree(callback_function, content=content, skip_projects=skip_projects)
        if not skip_projects:
            for project in self.projects:
                callback_function(project, content=content)
