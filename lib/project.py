# SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import logging

from lib.member import Member
from lib.sharedwithgroup import SharedWithGroup

class Project:

    def __init__(self, gl, project_id, expected_namespace = None):
        self.gl = gl
        self.id = project_id
        self.obj = gl.projects.get(self.id)
        self.name = self.obj.name
        self.web_url = self.obj.web_url
        # Lets filter projects that are not within the (parent) groups namespace
        # i.e. all projects that the parent group is used to provide access to
        # the project.
        if expected_namespace and not self.obj.namespace['full_path'] == expected_namespace:
            logging.info(f"Skipping project - not in required namespace: {expected_namespace} != {self.obj.namespace['full_path']}")
            self.in_expected_namespace = False
        else:
            logging.info(f"Processing project {self.name}")
            self.in_expected_namespace = True
            self.member_users = Member(gl, self.obj)
            self.member_groups = []
            for shared_with_group in self.obj.shared_with_groups:
                self.member_groups.append(SharedWithGroup(shared_with_group))

            # https://docs.gitlab.com/ee/api/project_access_tokens.html
            self.accesstokens = []
            for accesstoken in self.obj.access_tokens.list():
                if accesstoken.active and not accesstoken.revoked:
                    self.accesstokens.append(accesstoken)

            # https://docs.gitlab.com/ee/api/deploy_tokens.html#project-deploy-tokens
            self.deploytokens = []
            for deploytoken in self.obj.deploytokens.list():
                if not deploytoken.revoked and not deploytoken.expired:
                    self.deploytokens.append(self.obj.deploytokens.get(deploytoken.id))

            # https://docs.gitlab.com/ee/api/deploy_keys.html#list-deploy-keys-for-project
            self.keys = []
            for key in self.obj.keys.list():
                self.keys.append(self.obj.keys.get(key.id))

            logging.debug(f"- {self.name} User Members: "+str(sum(1 for _ in self.member_users)))
            logging.debug(f"- {self.name} Group Members: "+str(sum(1 for _ in self.member_groups)))

    def __str__(self):
        return(f"id: {self.id}; name: {self.name}")

