# SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

class SharedWithGroup:

    def __init__(self, dict):
        self.id = dict['group_id']
        self.name = dict['group_name']
        self.group_full_path = dict['group_full_path']
        self.group_access_level = dict['group_access_level']
        self.expires_at = dict['expires_at']
