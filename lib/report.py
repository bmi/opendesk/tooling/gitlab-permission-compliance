# SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-indentifier: Apache-2.0

import sys
import gitlab
import logging
from datetime import datetime, timedelta

class Report:

    def __init__(self, context = 'no ctx', baseurl = '', gitlab_accesstoken = ''):
        logging.debug("Initialize Report-Object")
        self.gl = gitlab.Gitlab(url=baseurl, private_token=gitlab_accesstoken)
        self.report = {}
        self.baseurl = baseurl
        self.context_prefix = context+' '
        self.timestamp = datetime.now().strftime("%Y-%m-%d %H:%M")
        self.userobjects = {}
        self.useremails = {}

    # Ideally we want here the non public email addresses of a user but they can only be accessed with
    # an admin account: https://docs.gitlab.com/ee/api/users.html#list-emails-for-user
    def __get_user_attribute(self, id, attribute):
        if id not in self.userobjects.keys():
            self.userobjects[id] = self.gl.users.get(id)
        return getattr(self.userobjects[id],attribute)

    # Adding an entry to the report dict
    def add(self, category = 'INFO', type = 'DEFAULT', parent = None, child = None, text = None):
        if category not in self.report.keys():
            self.report[category] = {}
        if type not in self.report[category]:
            self.report[category][type] = {}
        if parent not in self.report[category][type]:
            self.report[category][type][parent] = {}
        self.report[category][type][parent][child] = text

    # Generating a single line of the report out of the give object's information
    def __get_object_string(self, object, indent = '', text = None):
        text = '' if text is None else f": {text}"
        name = ''
        if hasattr(object, 'name'):
            name = object.name
        elif hasattr(object, 'title'):
            name = object.title
        else:
            sys.exit(f"Did not find name attribute that can be used for name in object of class {type(object).__name__}")
        if 'Member' in type(object).__name__:
            return f"{indent}- User [{object.name}]({object.web_url}) ({object.username} / {object.id} / {self.__get_user_attribute(object.id, 'public_email')}){text}"
        elif 'SharedWithGroup' in type(object).__name__:
            return f"{indent}- Group [{object.name}]({self.baseurl}/{object.group_full_path}) ({object.id}){text}"
        else:
            if hasattr(object, 'web_url'):
                return f"{indent}- [{type(object).__name__} {name}]({object.web_url}) ({object.id}){text}"
            else:
                return f"{indent}- **{type(object).__name__}** {name} ({object.id}){text}"

    # Adds a single headline and the headline content's within an issue/report
    def __generate_category_output(self, category):
        output = []
        for type in self.report[category]:
            output.append(f"## {type}")
            for parent in self.report[category][type]:
                output.append(self.__get_object_string(parent))
                for child in self.report[category][type][parent]:
                    output.append(self.__get_object_string(child, indent = "  ", text = self.report[category][type][parent][child]))
        return "\n".join(output)

    # Creates the
    def create_issues(self, gl_project, label_map = {}, title_map = {}):
        for category in self.report:
            title = f"{title_map[category]} {self.timestamp}" if category in title_map else f"{category} - {self.timestamp}"
            labels = label_map[category] if category in label_map else None
            issue_content = self.__generate_category_output(category)
            gl_project.issues.create({'title': self.context_prefix+title,
                               'description': issue_content,
                               'labels': labels})

    # prints the report to STDOUT
    def print(self):
        for category in self.report:
            print(f"# {category}")
            print(self.__generate_category_output(category))

    # cleans up already closed (reporting) issues that are `days` days old
    def cleanup_closed(self, gl_project, days = 0):
        num_days = int(days)
        if num_days > 0:
            day_date = datetime.today()-timedelta(days=num_days)
            day_string = day_date.strftime("%Y-%m-%d")
            closed_issues = gl_project.issues.list(state='closed', updated_before=day_string)
            logging.debug(f"Found {len(closed_issues)} issues with state closed and last update after {day_string}, going to delete them now")
            for issue in closed_issues:
                issue.delete()
