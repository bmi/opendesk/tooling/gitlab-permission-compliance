# SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import logging

class Member:
    # https://docs.gitlab.com/ee/api/members.html#roles
    # https://python-gitlab.readthedocs.io/en/v4.1.1/gl_objects/groups.html#group-members

    def __init__(self, gl, object):
        logging.debug(f"Init Members")
        self.gl = gl
        self.obj = object
        self.members = []
        for member in self.obj.members.list(get_all=True):   # gets (all) direct members
            if hasattr(member, 'created_by'):    # Assuption: The member that has no "created by" entry was the user that created to group/project
                if member.state == 'active' and not member.locked:
                    self.members.append(member)
                    logging.debug(f"Member: {member.id}; {member.username}; {member.name}; {member.access_level}")
                else:
                    logging.debug(f"Skipping {member.id}; {member.username}; {member.name} due state 'inactive' or locked-out")
            else:
                logging.debug(f"Skipping {member.id}; {member.username}; {member.name} as creator of object or bot (no created_by details)")

    def __iter__(self):
        return self.members.__iter__()

    def __next__(self):
        return self.members.__next__()
