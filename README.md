<!--
SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

<h1>Gitlab Permission Compliance</h1>

* [Disclaimer](#disclaimer)
* [Need to know about GitLab permissions](#need-to-know-about-gitlab-permissions)
* [Group permission management concept and compliance rules](#group-permission-management-concept-and-compliance-rules)
  * [Organisation](#organisation)
  * [Functions](#functions)
  * [Everyone](#everyone)
* [Compliance script](#compliance-script)
  * [Checks](#checks)
  * [Actions](#actions)
  * [Renders / reports](#renders--reports)
* [Setup](#setup)

# Disclaimer

This repo describes a concept for managing permissions within GitLab as well as a script to support this concept in terms of automation and compliance.

# Need to know about GitLab permissions

- Permissions are inherited downwards within the group/project structure or in other words: When you get permission to group *A* you have the same permission to all subgroups and projects of *A*.
- Permissions do not support nested groups:
  - When assigning a permission group to a work group (or project) only the users that are direct members of the permission group will be granted the set access level.
  - Subgroups or membergroups of the permission group do not get any permissions themselves.
- When managing permission using groups the lower permission will "win". Two examples:
  - You have "Maintainer" permission in permission group "Project Maintainers" and that group is set as member group of "Our development project" with permission "Guest" it will result in Guest permissions on "Our development project".
  - You have "Developer" permission in permission group "Everyone" and that group is set as member group of "Workgroup Root" with permission "Maintainer" it will result in Developer permissions for you on "Workgroup Root".
- It requires "Owner" permissions to manage group memberships.

# Group permission management concept and compliance rules

- We use a group structure to manage permissions (the `permission tree`) that is separated from the structure where the actual work happens (the `work tree`).
  - "Separated" in that context means that neither the permission tree is allowed to be a subtree of the work tree nor the other way around. So they are usually parallel (sub)trees of the a base/root group.
- We always manage permissions using groups, direct user assignments to a project/group in the work tree are not allowed.
- On the first sublevel of the permission groups you have three subgroups:
  - Organisation
  - Functions
  - Everyone
- This gives you the ability for a matrix style setup.

## Organisation

Below this group you setup your organisational structure, every member of your organisation (or project) should be listed in here.

## Functions

Below this group you setup you functional structure.

## Everyone

Everyone from the `Organisation` will be part of this group. This is automanaged by the compliance script.

Note: The Everyone group should be assigned as "Guest" to the permission group root in order for the users to browse your organisational and functional setup.

# Compliance script

The script generates an overview of the assigned groups & permissions.

## Checks

- Permission groups
  - Checks that there are no doublette assignments within the `Organisation` branch.
  - Checks that all members within the `Function´ group branch are also members within the `Organisation` group.
- Work groups & projects
  - Checks that there are no direct members within the structure.

## Actions

- Ensures that the `Everone` group contains all members from the `Organsation` branch, but not more.

## Renders / reports

- Groups and Projects with
  - Members
  - Access token
  - Deploy token
  - Deploy keys

# Setup

**Note**: You need to use a single account to report on work/permission groups that has permission on both, otherwise e.g. on work branch the shared_with_groups (which usually come from the permission branch) cannot be reported upon.
**Note**: In order to query access tokens the script needs API access with `owner` priviledges. As users with accesslevel `maintainer` can [access CI variables](https://docs.gitlab.com/ee/ci/variables/#for-a-project) and [take over ownership of scheduled pipelines](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#take-ownership) you want to ensure that this script is executed in an area where people with maintainer access are fine to also get owner priviledges for the areas `owner` access tokens are provided for!
