#!/usr/bin/python3
# SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import gitlab
import logging
import configargparse

from lib.group import *
from lib.report import *

####################
## INITIALIZATION ##
####################

p = configargparse.ArgParser()
p.add('--context', env_var='CONTEXT', default='no context set', help='The context value will be added to the issue title - if you are checking multiple workgroups it is helpful to distinguish the reports/results.')
p.add('--gitlab_baseurl', env_var='GITLAB_BASEURL', default='https://gitlab.opencode.de', help='The https base url for the GitLab instance.')
p.add('--permission_base_namespace', env_var='PERMISSION_BASE_NAMESPACE', help='The namespace/path to your permission groups base which contains your orga and function group structure as well as the reporting project. If omitted the function group compliance checks are skipped.')
p.add('--permission_report_projectid', env_var='PERMISSION_REPORT_PROJECTID', required=True, help='The ID of the project the issues created by this script are created in.')
p.add('--work_base_namespace', env_var='WORK_BASE_NAMESPACE', help='The namespace to run the work group checks onto.')
p.add('--gitlab_accesstoken', env_var='GITLAB_ACCESSTOKEN', required=True, help='The access token to access the permission groups and workgroups, scope "API" and accesslevel "OWNER" required.')
p.add('--permission_groupname_organization', env_var='PERMISSION_GROUPNAME_ORGANIZATION', default='organizations', help='The name of the subgroup within the permission base namespace/group containing the org structure.')
p.add('--permission_groupname_functions', env_var='PERMISSION_GROUPNAME_FUNCTIONS', default='functions', help='The name of the subgroup within the permission base namespace/group containing the functions structure.')
p.add('--permission_groupname_everyone', env_var='PERMISSION_GROUPNAME_EVERYONE', default='everyone', help='The name of the subgroup within the permission base namespace/group containing the group for everyone (from the org structure) that is automaintained by this script.')
p.add('--only_manage_group_everyone', env_var='ONLY_MANAGE_GROUP_EVERYONE', default='no', help='If you only want to automanage the "everyone" group based on the orga-tree members set it to "yes". requires the permission_base_namespace to be provided')
p.add('--cleanup_closed_issue_days', env_var='CLEANUP_CLOSED_ISSUE_DAYS', default='0', help='Deletes closed issues created by the script (or more correct by the user executing the script) in the configured reporting project N days after their last update, 0 does not delete.')
#p.add('--permission_level_everyone', env_var='PERMISSION_LEVEL_EVERYONE', default='gitlab.const.AccessLevel.DEVELOPER', help='The permission level members of the group everyone get assgined.')
p.add('--loglevel', env_var='LOGLEVEL', default='WARNING', help='Set the loglevel: DEBUG, INFO, WARNING, ERROR, CRITICAL')
options = p.parse_args()

logging.basicConfig(format='%(asctime)s %(levelname)-5.5s %(message)s', level=options.loglevel)
logging.debug(options)
# for complete debugness you might want to enable the debuglevel on the gitlab objects `gl_*.enable_debug()`

report = Report(context = options.context, baseurl=options.gitlab_baseurl, gitlab_accesstoken=options.gitlab_accesstoken)

access_level_map = {}
for key, value in gitlab.const.AccessLevel._member_map_.items():
    access_level_map[value] = key

##########################
# report: users and keys #
##########################
def list_configured_permissions(object, **_):
    global branch_title
    for member_group in object.member_groups:
        report.add("INFO", f"Assigned {branch_title}", object, member_group, f"as {access_level_map[member_group.group_access_level]}")
    for member_user in object.member_users:
        report.add("INFO", f"Assigned {branch_title}", object, member_user, f"as {access_level_map[member_user.access_level]} by {member_user.created_by['name']}")
    for accesstoken in object.accesstokens:
        report.add("INFO", f"Assigned {branch_title}", object, accesstoken, f"as {access_level_map[accesstoken.access_level]}, Created: {accesstoken.created_at[0:10]}, Expires: {accesstoken.expires_at}, Scopes: [{', '.join(accesstoken.scopes)}]")
    if hasattr(object, 'keys'):
        for key in object.keys:
            report.add("INFO", f"Assigned {branch_title}", object, key, f"Created at: {key.created_at}, Expires: {key.expires_at[0:10]}, Can push: {key.can_push}")
    if hasattr(object, 'deploytokens'):
        for token in object.deploytokens:
            expires = token.expires_at[0:10] if token.expires_at is not None else 'n/a'
            report.add("INFO", f"Assigned {branch_title}", object, token, f"Username: {token.username}, Expires: {expires}, Scopes: [{', '.join(token.scopes)}]")

#################
## WORK GROUPS ##
#################
if not options.work_base_namespace:
    logging.warning(f"--work_base_namespace not defined, skipping work group check")
else:
    gl_work = gitlab.Gitlab(url=options.gitlab_baseurl, private_token=options.gitlab_accesstoken)
    work_groups_namespace = gl_work.namespaces.get(options.work_base_namespace)
    work_group_base = Group(gl_work, id = work_groups_namespace.id)

    ################################
    # check: no direct assignments #
    ################################
    def render_direct_assignments(object, **_):
        for member_user in object.member_users:
            report.add("ERROR", "Direct user assignment", object, member_user, f"as {access_level_map[member_user.access_level]}")
    work_group_base.process_tree(render_direct_assignments)

    ##########################
    # report: users and keys #
    ##########################
    branch_title = 'in Work Tree'
    work_group_base.process_tree(list_configured_permissions)

#######################################
## PERMISSION GROUPS & ISSUE PROJECT ##
#######################################
gl_perm = gitlab.Gitlab(url=options.gitlab_baseurl, private_token=options.gitlab_accesstoken)
issues_project = gl_perm.projects.get(options.permission_report_projectid)

if not options.permission_base_namespace:
    logging.warning(f"--permission_base_namespace not defined, skipping permission group check - and therefore can also not manage the '{options.permission_groupname_everyone}' group")
else:
    orga_groups_path = options.permission_base_namespace+'/'+options.permission_groupname_organization
    function_groups_path = options.permission_base_namespace+'/'+options.permission_groupname_functions
    everyone_group_path = options.permission_base_namespace+'/'+options.permission_groupname_everyone

    orga_group_base_namespace = gl_perm.namespaces.get(orga_groups_path)
    orga_group_base = Group(gl_perm, id = orga_group_base_namespace.id)

    everyone_group_base_namespace = gl_perm.namespaces.get(everyone_group_path)
    everyone_group_base = Group(gl_perm, id = everyone_group_base_namespace.id)

    ########################################
    # GENERIC FUNCTIONS                    #
    ########################################
    # load all userids
    def load_userid_map(object, content):
        for member_user in object.member_users:
            content[member_user.id] = member_user
    # Initialize first as the object is modified by call-by-reference and not returned from the function
    orga_userid_map = {}
    orga_group_base.process_tree(load_userid_map, content=orga_userid_map)
    everyone_userid_map = {}
    everyone_group_base.process_tree(load_userid_map, content=everyone_userid_map)

    #############################################################
    # ACT: ENSURE ALL ORGA USERS ARE ASSIGNED IN GROUP EVERYONE #
    # BUT REMOVE MEMBERS FROM EVERYONE WHEN NO ORGA ASSIGNMENT! #
    #############################################################
    # add to everyone
    for user_id in [i for i in orga_userid_map.keys() if i not in everyone_userid_map.keys()]:
        logging.debug(f"Adding user {user_id} to group '{options.permission_groupname_everyone}'")
        try:
            everyone_group_base.obj.members.create({
                'user_id': user_id,
                'access_level': gitlab.const.AccessLevel.DEVELOPER
            })
            report.add("ACTION", f"User added to group {options.permission_groupname_everyone}", everyone_group_base, orga_userid_map[user_id])
        except gitlab.exceptions.GitlabCreateError as e:
            if e.response_code == 409 and e.error_message == 'Member already exists':
                logging.debug(f"User {user_id} already exists in group '{options.permission_groupname_everyone}'.")
            elif e.response_code == 400 and 'should be greater than or equal' in str(e.error_message) and 'inherited membership from' in str(e.error_message):
                logging.debug(f"User {user_id} is already member of a parent-group with higher priviledges.")
            else:
                logging.error(f"Unhandled error while adding user {user_id} to group '{options.permission_groupname_everyone}' - code: {e.response_code}, message: {e.error_message}, body: {e.response_body} ")
                raise gitlab.exceptions.GitlabCreateError
    # remove from everyone
    for user_id in [i for i in everyone_userid_map.keys() if i not in orga_userid_map.keys()]:
        logging.debug(f"Removing user {user_id} from group '{options.permission_groupname_everyone}'")
        everyone_group_base.obj.members.delete(user_id)
        report.add("ACTION", f"User removed from group {options.permission_groupname_everyone}", everyone_group_base, everyone_userid_map[user_id])

    ## Report and Checks
    if options.only_manage_group_everyone is 'yes':
        logging.info(f"Script was started with 'only_manage_group_everyone' option, skipping the other permission tree activities")
    else:
        function_group_base_namespace = gl_perm.namespaces.get(function_groups_path)
        function_group_base = Group(gl_perm, id = function_group_base_namespace.id)

        ##########################
        # report: users and keys #
        ##########################
        branch_title = 'in Orga Tree'
        orga_group_base.process_tree(list_configured_permissions)
        branch_title = 'in Function Tree'
        function_group_base.process_tree(list_configured_permissions)

        ########################################
        # check: all func users in org defined #
        ########################################
        def check_userid_map(object, content):
            for member_user in object.member_users:
                if member_user.id not in content:
                    report.add("ERROR", "No Orga assigned", object, member_user, f"as {access_level_map[member_user.access_level]}")
        function_group_base.process_tree(check_userid_map, content=orga_userid_map)

        ###################################
        # check: no doublettes org branch #
        ###################################
        def check_user_doublette_map(object, content):
            type_string = 'Doublette User Assignment in Org-Tree'
            for member_user in object.member_users:
                if member_user.id not in content:
                    content[member_user.id] = [ object ]
                else:
                    if len(content[member_user.id]) == 1:
                        report.add("ERROR", type_string, member_user, content[member_user.id][0], f"as {access_level_map[member_user.access_level]}")
                    report.add("ERROR", type_string, member_user, object, f"as {access_level_map[member_user.access_level]}")
                    content[member_user.id].append(object)
        user_doublette_map = {}
        orga_group_base.process_tree(check_user_doublette_map, content=user_doublette_map)

        ###############################
        # check: projects not allowed #
        ###############################
        def check_for_projects(object, **_):
            if type(object).__name__ == 'Project':
                report.add("ERROR", "Project Not Allowed", object)
        orga_group_base.process_tree(check_for_projects)
        function_group_base.process_tree(check_for_projects)

        ############################
        # check: user access level #
        ############################
        def check_user_accesslevel(object, content):
            for member_user in object.member_users:
                if member_user.access_level < content:
                    report.add("WARNING", "User Access Level Low", object, member_user, f"{access_level_map[member_user.access_level]} < {access_level_map[content]}")
                elif member_user.access_level > content:
                    report.add("ERROR", "User Access Level High", object, member_user, f"{access_level_map[member_user.access_level]} > {access_level_map[content]}")
        orga_group_base.process_tree(check_user_accesslevel, content=gitlab.const.AccessLevel.MAINTAINER)
        function_group_base.process_tree(check_user_accesslevel, content=gitlab.const.AccessLevel.MAINTAINER)
        everyone_group_base.process_tree(check_user_accesslevel, content=gitlab.const.AccessLevel.DEVELOPER)

# For local development or debugging purposes you may not want to create the issues, so just use:
#report.print()
report.create_issues(issues_project,
                     label_map = { 'ERROR': [ 'Severity: ERROR' ], 'WARNING': [ 'Severity: WARNING' ] },
                     title_map = { 'INFO': 'Reporting at '}
                     )
report.cleanup_closed(issues_project, options.cleanup_closed_issue_days)
